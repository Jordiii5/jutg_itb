import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.io.path.Path
import kotlin.io.path.readLines

class Problemas {
    val problemes = mutableListOf<Problema>()

    init{
        val problemasJson = Path("./src/main/kotlin/problemas.json").readLines()
        for(problemaJson in problemasJson){
            val problema = Json.decodeFromString<Problema>(problemaJson)
            this.problemes.add(problema)
        }
    }

    fun resueltos(no:Boolean):List<Problema>{
        val resueltos = mutableListOf<Problema>()
        val noResueltos = mutableListOf<Problema>()
        for(problema in this.problemes){
            if(problema.resuelto) resueltos.add(problema)
            else noResueltos.add(problema)
        }
        return if(no) noResueltos
        else resueltos
    }

    fun ordenarPor(tema:String):List<Problema>{
        val ordenados = mutableListOf<Problema>()
        for(problema in this.problemes){
            if(problema.tema == tema) ordenados.add(problema)
        }
        return ordenados
    }
}