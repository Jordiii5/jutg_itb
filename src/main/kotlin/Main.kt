import java.util.Scanner
import kotlin.system.exitProcess

val scanner = Scanner(System.`in`)
// Definir una clase de modelo para Alumno
const val redBold = "\u001B[1;31m"
const val red = "\u001B[5;31m"
const val reset = "\u001B[0m"
const val white = "\u001B[1;97m"
const val box = "\u001b[51m"
const val bold = "\u001b[1m"
const val green = "\u001b[38;5;10m"
const val orangebold = "\u001B[1;33m"
const val orange = "\u001B[43m"
const val black = "\u001B[1;30m"
const val mgreen = "\u001B[2m"
const val blue = "\u001B[96m"
const val pink = "\u001B[95m"
var problemasResueltos: Int = 0
var contadorproblemas: Int = 0
var i: Int = 0

fun main(){
    println(""""$white
 ____                                                       __                            _____           __           ______  ______  ____      
/\  _`\    __                                        __    /\ \                          /\___ \         /\ \__       /\__  _\/\__  _\/\  _`\    
\ \ \L\ \ /\_\     __    ___   __  __     __    ___ /\_\   \_\ \    ___          __      \/__/\ \  __  __\ \ ,_\    __\/_/\ \/\/_/\ \/\ \ \L\ \  
 \ \  _ <'\/\ \  /'__`\/' _ `\/\ \/\ \  /'__`\/' _ `\/\ \  /'_` \  / __`\      /'__`\       _\ \ \/\ \/\ \\ \ \/  /'_ `\ \ \ \   \ \ \ \ \  _ <' 
  \ \ \L\ \\ \ \/\  __//\ \/\ \ \ \_/ |/\  __//\ \/\ \ \ \/\ \L\ \/\ \L\ \    /\ \L\.\_    /\ \_\ \ \ \_\ \\ \ \_/\ \L\ \ \_\ \__ \ \ \ \ \ \L\ \
   \ \____/ \ \_\ \____\ \_\ \_\ \___/ \ \____\ \_\ \_\ \_\ \___,_\ \____/    \ \__/.\_\   \ \____/\ \____/ \ \__\ \____ \/\_____\ \ \_\ \ \____/
    \/___/   \/_/\/____/\/_/\/_/\/__/   \/____/\/_/\/_/\/_/\/__,_ /\/___/      \/__/\/_/    \/___/  \/___/   \/__/\/___L\ \/_____/  \/_/  \/___/ 
                                                                                                                    /\____/                      
                                                                                                                    \_/__/                       
$reset""")
    println("${box}$white 1- INICIAR SESIÓN $reset  ${box}$white 2-CREAR CUENTA $reset")
    do{
        print("${mgreen}ELIGE UNA OPCIÓN VÁLIDA:$reset ")
        var opcio = scanner.next()
        when(opcio){
            "1" -> {
                iniciarsesion()
            }
            "2" -> {
                crearcuenta()
            }
            else -> opcio = "error"
        }
    }while (opcio == "error")
}

fun iniciarsesion(){
    println("${white}IDENTIFICATE:$reset")
    println("     ${box}$white 1- ALUMNO $reset     ${box}$white 2- PROFESOR $reset")
    do{
        print("${mgreen}ELIGE UNA OPCIÓN VÁLIDA: ")
        var opcioUsuari = scanner.next()
        when(opcioUsuari){
            "1" -> {
                val alumno = Alumno("","")
                alumno.iniciarsesion()
            }
            "2" -> {
                val profesor = Profesor("", "", "")
                profesor.iniciarsesion()
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
}

fun crearcuenta(){
    println("${white}IDENTIFICATE:")
    println("     ${box}${white} 1- ALUMNO $reset    ${box}${white} 2- PROFESOR $reset")
    do{
        print("${mgreen}ELIGE UNA OPCIÓN VÁLIDA: ")
        var opcioUsuari = scanner.next()
        when(opcioUsuari){
            "1" -> {
                println("${white}Introduce tu nombre")
                val nombre = scanner.next()
                println("Introduce tu mail$reset")
                val mail = scanner.next()
                val alumno = Alumno("$nombre","$mail")
                alumno.crearcuenta()
                println("${white}Dirigido a la pantalla de iniciar sesion$reset")
                alumno.iniciarsesion()
            }
            "2" -> {
                println("${white}Introduce tu nombre")
                val nombre = scanner.next()
                println("Introduce tu mail")
                val mail = scanner.next()
                println("Introduce tu contraseña$reset")
                val contraseña = scanner.next()
                val profesor = Profesor("$nombre", "$mail", "$contraseña")
                profesor.crearcuenta()
                println("${white}Dirigido a la pantalla de iniciar sesion$reset")
                profesor.iniciarsesion()
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
}



