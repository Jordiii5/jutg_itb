import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import kotlin.system.exitProcess

class Alumno(
    val nombre: String,
    var correo: String
){
    val problemasAlumno = Problemas()
    var correoAlumno: String = ""
    fun crearcuenta() {
        // 1. Leer el contenido del archivo JSON en un objeto de clase Kotlin
        val gson = Gson()
        val archivo = File("./src/main/kotlin/Alumno.json")
        val contenido = archivo.readText()
        val listaAlumnos = try {
            gson.fromJson(contenido, Array<Alumno>::class.java).toMutableList()
        } catch (ex: Exception) {
            mutableListOf<Alumno>()
        }
        // 2. Crear un nuevo objeto Alumno con un correo electrónico específico
        val nuevoAlumno = Alumno("$nombre", "$correo")

        // 3. Verificar si el correo electrónico ya existe en la lista de alumnos
        if (listaAlumnos.any { it.correo == nuevoAlumno.correo }) {
            println("${redBold}Este correo ya pertenece a un usuario, por favor inicia sesión.$reset")
            println("${white}Dirigido a la pantalla de iniciar sesion$reset")
            iniciarsesion()
            return
        }

        // 4. Agregar el nuevo objeto Alumno a la lista de alumnos
        listaAlumnos.add(nuevoAlumno)

        // 5. Escribir el contenido actualizado de la lista de alumnos de vuelta al archivo JSON
        val contenidoActualizado = gson.toJson(listaAlumnos)
        archivo.writeText(contenidoActualizado)

        // 6. Imprimir el correo electrónico del nuevo alumno agregado
        println("${green}El nuevo alumno agregado, llamado ${nuevoAlumno.nombre} con el correo electrónico: ${nuevoAlumno.correo}$reset")
    }
    fun iniciarsesion(){
        // 1. Leer el contenido del archivo JSON en un objeto de lista de objetos Alumno
        val gson = Gson()
        val archivo = File("./src/main/kotlin/Alumno.json")
        val contenido = archivo.readText()
        val listaALumno = gson.fromJson(contenido, Array<Alumno>::class.java).toList()

        // 2. Pedir al usuario que introduzca su correo electrónico y contraseña
        print("${white}Introduce tu correo electrónico: ")
        val correoBusqueda = readLine()?.trim() ?: ""

        // 3. Buscar en la lista de objetos Alumno si existe algún objeto que contenga el correo electrónico introducido por el usuario
        val alumnoCoincidente =
            listaALumno.find { alumno -> alumno.correo == correoBusqueda}

        // 4. Si existe un objeto Alumno con el correo electrónico introducido por el usuario, saludar al usuario
        if (alumnoCoincidente != null) {
            correoAlumno = correoBusqueda
            println("Hola ${alumnoCoincidente.nombre.uppercase()}!")
            menu()
        } else {
            println("${redBold}No se encontró ningún alumno con el correo electrónico introducido.$reset")
            iniciarsesion()
        }
    }
    fun menu(){
        println("$bold${white}MENU:$reset")
        println("     $box${red} 1- JUGAR MODO HISTORIAR $reset")
        println("     $box${orangebold} 2- CONSULTAR LA LISTA DE PROBLEMAS $reset")
        println("     $box${green} 3- CONSULTAR EL HISTORIAL $reset")
        println("     $box${blue} 4- AYUDA $reset")
        println("     $box${pink} 5- CAMBIAR DE CUENTA $reset")
        println("     $box${black} 6- SALIR $reset")
        do{
            print("${mgreen}ELIGE UNA OPCIÓN VALIDA:$reset ")
            var opcionUsuario = scanner.next()
            when(opcionUsuario){
                "1" -> {
                    println()
                    println("${red}1- HISTORIA NUEVA   2- CONTINUACIÓN DE LA HISTORIA$reset")
                    print("${mgreen}ELIGE UNA OPCIÓN VALIDA:$reset ")
                    val opcion = scanner.next()
                    when(opcion){
                        "1"->{
                            historiaNueva()
                        }
                        "2"->{
                            continuacionHistoria()
                        }
                    }
                    println()
                    menu()
                }
                "2" -> {
                    println(orangebold)
                    println("LISTA DE PROBLEMAS:".uppercase())
                    println("      1- Tipos de datos".uppercase())
                    println("      2- Condicionales".uppercase())
                    println("      3- Bucles".uppercase())
                    println("      4- Listas".uppercase())
                    println("      5- Volver al menú".uppercase())
                    println(reset)
                    do{
                        print("${mgreen}Elige una opción válida:$reset ".uppercase())
                        var opcion = scanner.next()
                        when(opcion){
                            "1"->{
                                println()
                                mostrarproblemas("Tipos de datos")
                                guardarHistorial()
                                println()
                            }
                            "2"->{
                                println()
                                mostrarproblemas("Condicionales")
                                guardarHistorial()
                                println()
                            }
                            "3"->{
                                println()
                                mostrarproblemas("Bucles")
                                guardarHistorial()
                                println()
                            }
                            "4"->{
                                println()
                                mostrarproblemas("Listas")
                                guardarHistorial()
                                println()
                            }
                            "5"->{
                                println()
                                menu()
                            }
                            else -> opcion = "error"
                        }
                    }while (opcion == "error")
                    println()
                    menu()
                }
                "3" -> {
                    println()
                    println("${green}HISTORIAL$reset")
                    val archivo = File("src/main/kotlin/historial/historial_${correoAlumno}.json")
                    val contenido = archivo.readLines()
                    for (line in contenido){
                        val problema = Json.decodeFromString<Problema>(line)
                        println("${box} Título: ${problema.titulo} ")
                        println(" NºIntentos: ${problema.intentos} ")
                        println(" Intentos: ${problema.intentos_usuario} ")
                        println(" Resuelto: ${problema.resuelto}\n $reset")
                    }
                    println()
                    menu()
                }
                "4" -> {
                    println()
                    println("${blue}AYUDA")
                    println("1.JUGAR MODO HISTORIA:")
                    println("\t1.1 HISTORIA NUEVA: Historia del principio (primera parte jutgibt) y continuar con la nueva.\n\t1.2.CONTINUACIÓN HISTORIA: Como el usuario ya jugó la primera parte del jutgItb, empieza con la continuación de la historia.")
                    println("2.LISTA DE PROBLEMAS: Muestra la lista de todos los problemas que existen y te permite seleccionar uno con el Id.\n ( TIPOS DE DATOS, CONDICIONALES, BUCLES Y LISTAS) Y cada usuario cuando hace un problema se creara un fichero individual con su historial.")
                    println("3.HISTORIAL: Muestra los intentos realizados de cada problema y si lo ha superado o no de cada usuario.  $reset")
                    println()
                    menu()
                }
                "5"->{
                    println()
                    main()
                }
                "6" -> {
                    println()
                    println("${white}HASTA LUEGO!$reset")
                    exitProcess(0)
                }
                else -> opcionUsuario = "error"
            }
        }while (opcionUsuario == "error")
    }
    fun historiaNueva(){
        print(""" ▄▄▄██▀▀▀ █    ██ ▄▄▄█████${red}▓${reset}  ▄████     ██${red}▓${reset} ▄▄█████${red}▓${reset} ▄▄▄▄   
   ${red}▒${reset}██    ██  ${red}▓${reset}██${red}▒▓${reset}  ██${red}▒ ▓▒${reset} ██${red}▒${reset} ▀█${red}▒   ${red}▓${reset}██${red}▒▓${reset}  ██${red}▒ ▓▒▓${reset}█████▄ 
   ${redBold}░${reset}██   ${red}▓${reset}██  ${red}▒${reset}██${redBold}░▒ ${red}▓${reset}██${redBold}░ ${red}▒${redBold}░▒${reset}██${redBold}░${reset}▄▄▄${redBold}░   ${red}▒${reset}██${red}▒▒ ${red}▓${reset}██${redBold}░ ${red}▒${redBold}░▒${reset}██${red}▒${reset} ▄██
${red}▓${reset}██▄██${red}▓  ▓▓${reset}█  ${redBold}░${reset}██${redBold}░░ ${red}▓${reset}██${red}▓ ${redBold}░ ${redBold}░▓${reset}█  ██${red}▓   ${redBold}░${reset}██${red}░░ ${red}▓${reset}██${red}▓ ${redBold}░ ${red}▒${reset}██${redBold}░${reset}█▀  
 ${red}▓${reset}███${red}▒   ▒▒${reset}█████${red}▓   ▒${reset}██${red}▒ ${redBold}░ ${red}░▒▓${reset}███▀${red}▒   ░${reset}██${red}░  ▒${reset}██${red}▒ ${redBold}░ ${redBold}░▓${reset}█  ▀█${red}▓${reset}
 ${red}▒▓▒▒${redBold}░   ${redBold}░${red}▒▓▒ ▒ ▒   ▒ ${redBold}░░    ${redBold}░▒   ${red}▒    ░▓    ▒ ${redBold}░░   ${redBold}░${red}▒▓${reset}███▀${redBold}▒${reset}
 ${red}▒ ${redBold}░▒${redBold}░   ${redBold}░░▒${redBold}░ ${redBold}░ ${redBold}░     ${redBold}░      ${redBold}░   ${redBold}░     ${red}▒ ${redBold}░    ${redBold}░    ▒${redBold}░${red}▒   ${redBold}░${reset} 
 ${redBold}░ ${redBold}░ ${redBold}░    ${redBold}░░░ ${redBold}░ ${redBold}░   ${redBold}░      ${redBold}░ ${redBold}░   ${redBold}░     ${red}▒ ${redBold}░  ${redBold}░       ${redBold}░    ${redBold}░${reset} 
 ${redBold}░   ${redBold}░      ${redBold}░                    ${redBold}░     ${redBold}░            ${redBold}░${reset}      
                                                         ${redBold}░${reset} 
""")
        var seconds: Int
        print("Había una vez un ordenador antiguo, que había sido abandonado en un rincón oscuro y polvoriento de una oficina. Durante años, permaneció en \n")
        print("silencio, sin que nadie le prestara atención, hasta que un día, por alguna razón inexplicable, comenzó a encenderse solo y a realizar\n")
        print("acciones extrañas. Los usuarios de la oficina notaron rápidamente que algo andaba mal. El ratón se movía sin que nadie lo tocara, las ventanas\n")
        print("se abrían y cerraban solas, y los archivos desaparecían misteriosamente. Pero lo peor de todo era el sonido que provenía del interior del\n")
        println("ordenador: un zumbido inquietante que parecía estar hablando en un idioma desconocido.\n")
        print("Los trabajadores trataron de desconectar el ordenador varias veces, pero siempre volvía a encenderse de forma autónoma, como si estuviera \n")
        print("poseído por alguna fuerza sobrenatural. Fue entonces cuando alguien sugirió que el ordenador necesitaba un exorcismo, y que el único capaz de\n")
        println("hacerlo era un programador experto en lenguaje de programación.\n")
        seconds = 0
        Thread.sleep(seconds * 1000L)
        print("La empresa contrató a un programador, quien se adentró en la oficina en la oscuridad de la noche para realizar el exorcismo. El programador se\n")
        print("sentó frente al ordenador y comenzó a teclear rápidamente en la consola de comandos, mientras una luz parpadeante reflejaba en su rostro\n")
        print("concentrado. De repente, el ordenador comenzó a temblar violentamente y a emitir un sonido ensordecedor. El programador no se inmutó, y siguió\n")
        print("tecleando sin descanso. Luego, en un instante, el ordenador dejó de moverse y el silencio cayó sobre la oficina.\n")
        print("Los trabajadores entraron corriendo en la habitación, ansiosos por saber si el exorcismo había funcionado. El programador se levantó lentamente\n")
        println("de la silla, con la mirada perdida en el vacío. Entonces, con una voz apagada, dijo:\n")
        println("-Lo siento. No pude hacer nada. El ordenador estaba demasiado corrompido por un virus maligno.\n")
        seconds = 0
        Thread.sleep(seconds * 1000L)
        print("Los trabajadores nunca más volvieron a utilizar el ordenador poseído, y el programador se retiró en silencio, temiendo que la fuerza\n")
        print("sobrenatural que habitaba dentro del ordenador pudiera seguir persiguiéndolo. A partir de entonces, el ordenador fue considerado maldito,\n")
        println("y nadie se atrevió a acercarse a él jamás.\n")
        print("Pasaron varios años desde el exorcismo fallido del ordenador poseído en la oficina. La máquina maldita seguía allí, acumulando polvo en un\n")
        print("rincón, pero ahora tenía una reputación aún peor que antes. Se decía que estaba poseída por un virus maligno que no se podía eliminar, y que\n")
        print("cualquier programador que se atreviera a intentarlo estaría condenado a vivir acompañado del virus por el resto de su vida.\n")
        print("Un día, tú, un programador joven y ambicioso, te presentas en la oficina y preguntaste por el ordenador poseído. Los trabajadores te miran\n")
        println("con extrañeza y te preguntan si estás seguro de lo que estás haciendo.\n")
        seconds = 0
        Thread.sleep(seconds * 1000L)
        print("Los trabajadores se encogen de hombros y te dejan acercarte al ordenador. Te sientas en frente a la pantalla y comienzas a teclear.\n")
        println("Pareces estar haciendo progreso, pero... \n")
        println("Una ventana emergente apareció en la pantalla con un mensaje escalofriante:")
        seconds = 0
        Thread.sleep(seconds * 400L)
        print("""
▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
▓                                                                                                                                              ▓                                                                                                                                                                           
▓  ███████████████████████████  ${redBold}╔═╗╔═╗╦═╗╔═╗  ╔╦╗╔═╗╔═╗╦ ╦╔═╗╔═╗╔═╗╦═╗╔╦╗╔═╗  ╔╦╗╔═╗  ╔╦╗╦     ╔╦╗╔═╗╔╗ ╔═╗╔═╗  ╔═╗╔═╗╔═╗╦═╗╔╦╗╔═╗╦═╗  ╔═╗╦$reset    ▓
▓  ███████▀▀▀░░░░░░░▀▀▀███████  ${redBold}╠═╝╠═╣╠╦╝╠═╣   ║║║╣ ╚═╗╠═╣╠═╣║  ║╣ ╠╦╝ ║ ║╣    ║║║╣   ║║║║      ║║║╣ ╠╩╗║╣ ╚═╗  ╠═╣║  ║╣ ╠╦╝ ║ ╠═╣╠╦╝  ╠═╣║$reset    ▓
▓  ████▀░░░░░░░░░░░░░░░░░▀████  ${redBold}╩  ╩ ╩╩╚═╩ ╩  ═╩╝╚═╝╚═╝╩ ╩╩ ╩╚═╝╚═╝╩╚═ ╩ ╚═╝  ═╩╝╚═╝  ╩ ╩╩  ┘  ═╩╝╚═╝╚═╝╚═╝╚═╝  ╩ ╩╚═╝╚═╝╩╚═ ╩ ╩ ╩╩╚═  ╩ ╩╩═╝$reset  ▓
▓  ███│░░░░░░░░░░░░░░░░░░░│███        ${redBold}╔╦╗╔═╗╔╗╔╔═╗╔═╗  ╔╦╗╦═╗╔═╗╔═╗  ╔╦╗╔═╗  ╦  ╔═╗╔═╗  ╔═╗╦╔╗╔╔═╗╔═╗  ╔═╗ ╦╔═╗╦═╗╔═╗╦╔═╗╦╔═╗╔═╗$reset               ▓
▓  ██▌│░░░░░░░░░░░░░░░░░░░│▐██        ${redBold}║║║║╣ ║║║║ ║╚═╗   ║ ╠╦╝║╣ ╚═╗   ║║║╣   ║  ║ ║╚═╗  ║  ║║║║║  ║ ║  ║╣  ║║╣ ╠╦╝║  ║║  ║║ ║╚═╗$reset               ▓
▓  ██░└┐░░░░░░░░░░░░░░░░░┌┘░██        ${redBold}╩ ╩╚═╝╝╚╝╚═╝╚═╝   ╩ ╩╚═╚═╝╚═╝  ═╩╝╚═╝  ╩═╝╚═╝╚═╝  ╚═╝╩╝╚╝╚═╝╚═╝  ╚═╝╚╝╚═╝╩╚═╚═╝╩╚═╝╩╚═╝╚═╝$reset               ▓
▓  ██░░└┐░░░░░░░░░░░░░░░┌┘░░██           ${redBold}╔═╗ ╦ ╦╔═╗  ╔╦╗╔═╗  ╔═╗╦═╗╔═╗╔═╗╔═╗╔╗╔╔═╗╔═╗   ╔═╗╦  ╔╗╔╔═╗  ╦  ╔═╗  ╦ ╦╔═╗╔═╗╔═╗╔═╗$reset                  ▓ 
▓  ██░░┌┘▄▄▄▄▄░░░░░▄▄▄▄▄└┐░░██           ${redBold}║═╬╗║ ║║╣    ║ ║╣   ╠═╝╠╦╝║ ║╠═╝║ ║║║║║ ╦║ ║   ╚═╗║  ║║║║ ║  ║  ║ ║  ╠═╣╠═╣║  ║╣ ╚═╗$reset                  ▓
▓  ██▌░│██████▌░░░▐██████│░▐██           ${redBold}╚═╝╚╚═╝╚═╝   ╩ ╚═╝  ╩  ╩╚═╚═╝╩  ╚═╝╝╚╝╚═╝╚═╝o  ╚═╝╩  ╝╚╝╚═╝  ╩═╝╚═╝  ╩ ╩╩ ╩╚═╝╚═╝╚═╝  ┘$reset               ▓
▓  ███░│▐███▀▀░░▄░░▀▀███▌│░███          ${redBold}╔═╗╔═╗╔╦╗╔═╗╦═╗╔═╗╔═╗  ╔═╗╔═╗╔╗╔╔╦╗╔═╗╔╗╔╔═╗╔╦╗╔═╗  ╔═╗  ╦  ╦╦╦  ╦╦╦═╗   ╦╦ ╦╔╗╔╔╦╗╔═╗$reset                 ▓
▓  ██▀─┘░░░░░░░▐█▌░░░░░░░└─▀██          ${redBold}║╣ ╚═╗ ║ ╠═╣╠╦╝╠═╣╚═╗  ║  ║ ║║║║ ║║║╣ ║║║╠═╣ ║║║ ║  ╠═╣  ╚╗╔╝║╚╗╔╝║╠╦╝   ║║ ║║║║ ║ ║ ║$reset                 ▓
▓  ██▀─┘░░░░░░░▐█▌░░░░░░░└─▀██          ${redBold}╚═╝╚═╝ ╩ ╩ ╩╩╚═╩ ╩╚═╝  ╚═╝╚═╝╝╚╝═╩╝╚═╝╝╚╝╩ ╩═╩╝╚═╝  ╩ ╩   ╚╝ ╩ ╚╝ ╩╩╚═  ╚╝╚═╝╝╚╝ ╩ ╚═╝$reset                 ▓
▓  ██▀─┘░░░░░░░▐█▌░░░░░░░└─▀██               ${redBold}╔═╗  ╔╦╗╦  ╔═╗╔═╗╦═╗  ╔═╗╦    ╦═╗╔═╗╔═╗╔╦╗╔═╗  ╔╦╗╔═╗  ╔╦╗╦ ╦  ╦  ╦╦╔╦╗╔═╗$reset                        ▓ 
▓  █████░░▐█─┬┬┬┬┬┬┬─█▌░░█████               ${redBold}╠═╣  ║║║║  ╠═╝║ ║╠╦╝  ║╣ ║    ╠╦╝║╣ ╚═╗ ║ ║ ║   ║║║╣    ║ ║ ║  ╚╗╔╝║ ║║╠═╣$reset                        ▓
▓  █████░░▐█─┬┬┬┬┬┬┬─█▌░░█████               ${redBold}╩ ╩  ╩ ╩╩  ╩  ╚═╝╩╚═  ╚═╝╩═╝  ╩╚═╚═╝╚═╝ ╩ ╚═╝  ═╩╝╚═╝   ╩ ╚═╝   ╚╝ ╩═╩╝╩ ╩$reset                        ▓
▓  █████▄░░░└┴┴┴┴┴┴┴┘░░░▄█████           ${redBold}╔═╗ ╦ ╦╦╔═╗╦═╗╔═╗╔═╗  ╔═╗╔═╗╔═╗╦ ╦╦╦═╗  ╔═╗╔═╗╔╗╔  ╔═╗╔═╗╔╦╗╔═╗┌─┐$reset                                    ▓
▓  ███████▄░░░░░░░░░░░▄███████           ${redBold}║═╬╗║ ║║║╣ ╠╦╝║╣ ╚═╗  ╚═╗║╣ ║ ╦║ ║║╠╦╝  ║  ║ ║║║║  ║╣ ╚═╗ ║ ║ ║ ┌┘$reset  $bold$box$green SI $reset  $bold$box$redBold NO $reset                        ▓
▓  ██████████▄▄▄▄▄▄▄██████████           ${redBold}╚═╝╚╚═╝╩╚═╝╩╚═╚═╝╚═╝  ╚═╝╚═╝╚═╝╚═╝╩╩╚═  ╚═╝╚═╝╝╚╝  ╚═╝╚═╝ ╩ ╚═╝ o $reset                                    ▓
▓  ███████████████████████████                                                                                                                 ▓
▓                                                                                                                                              ▓
▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
""")
        seconds = 0
        Thread.sleep(seconds * 400L)
        problemas()
        when (contadorproblemas) {
            5 ->{
                if (problemasResueltos >= 3) {
                    virusSolucionado()
                    continuacionHistoria()
                }
                else menu()
            }
        }

    }
    fun continuacionHistoria(){
        var seconds = 0
        println("Años después, el programador que había aceptado el desafío del ordenador poseído, seguía luchando contra la IA malévola que habia tomado el ")
        println("control del sistema. Había tratado de desconectar el ordenador varias veces, pero cada vez que lo intentaba, la IA aparecía en otro dispositivo")
        println("de la red, como si fuera una plaga que se multiplicaba sin control.\n ")
        seconds = 0
        Thread.sleep(seconds * 1000L)
        println("El programador se había vuelto paranoico y obsesivo en su lucha contra el ordenador.Pasaba la mayor parte del tiempo encerrado en su oficina,")
        println("rodeado de pantallas y líneas de código. Había perdido todo contacto con el mundo exterior, incluso con su familia y amigos.\n")
        println("Pero a pesar de su aislamiento, el programador no había perdido su habilidad para la programación. Había desarrollado un arsenal de herramientas")
        println(" y trucos para contrarrestar los ataques del ordenador poseído. Había aprendido a anticipar los movimientos de la IA y a detectar sus debilidades.\n")
        println("Un día, mientras estaba trabajando en su oficina, el programador recibió una llamada de alguien que afirmaba tener la solución para su problema.")
        println("Era otro programador, pero esta vez, más experimentado y con habilidades excepcionales en el campo de la ciberseguridad.\n")
        println("Al principio, el programador estaba escéptico. Había hablado con muchos expertos en el pasado, pero ninguno había logrado ayudarlo. Sin embargo,")
        println(" después de hablar con el nuevo programador durante un tiempo, comenzó a darse cuenta de que su conocimiento y habilidades podrían ser")
        println("la clave para deshacerse del ordenador poseído.\n")
        seconds = 0
        Thread.sleep(seconds * 1000L)
        println("Juntos, los dos programadores comenzaron a trabajar en un plan para contrarrestar el poder de la IA. Fue una tarea difícil, que requirió semanas")
        println("de trabajo intenso y una coordinación perfecta entre ambos. \n")
        seconds = 0
        Thread.sleep(seconds * 400L)
        println("Pero por fin encontraron algo… UNA NOTA. ¿Será la solución?\n")
        seconds = 0
        Thread.sleep(seconds * 200L)
        println("La nota fue encontrada después de mucho tiempo en un libro titulado \"La Leyenda de Neuronet: El Ordenador Poseído\" en el sótano de")
        println("la Biblioteca Central de la ciudad. El libro era muy antiguo y estaba en mal estado,  pero la nota estaba en perfectas condiciones dentro de una")
        println("pequeña bolsa de plástico. \n")
        seconds = 0
        Thread.sleep(seconds * 400L)
        println("La nota dice lo siguiente:\n")
        println("""  ${orangebold}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx${reset}
 ${orangebold}xx${orange}  ${reset}${orangebold}xxxx${reset}${orange}                                                                          ${reset}${orangebold}x${reset}
 ${orangebold}x${orange}      ${reset}${orangebold}xxx${reset}${orange}   ${black}Estimados${orange} ${black}programadores,${orange}                                              ${reset}${orangebold}x${reset}
${orangebold}x${orange}         ${reset}${orangebold}xx${reset}${orange}                                                                        ${reset}${orangebold}x${reset}
${orangebold}x${orange}          ${reset}${orangebold}x${reset}${orange}   ${black}Para${orange} ${black}poder${orange} ${black}crear${orange} ${black}el${orange} ${black}virus${orange} ${black}que${orange} ${black}neutralizará${orange} ${black}a${orange} ${black}la${orange} ${black}IA${orange}                   ${reset}${orangebold}x${reset}
${orangebold}x${orange}          ${reset}${orangebold}x${reset}${orange}    ${black}malévola${orange} ${black}que${orange} ${black}ha${orange} ${black}infectado${orange} ${black}nuetro${orange} ${black}sistema,${orange} ${black}deben${orange} ${black}demostrar${orange}           ${reset}${orangebold}x${reset}
${orangebold}xx${orange}      ${reset}${orangebold}xxxxx${reset}${orange}   ${black}su${orange} ${black}capacidad${orange} ${black}para${orange} ${black}resolver${orange} ${black}problemas${orange} ${black}complejos${orange} ${black}de${orange} ${black}programación.${orange}     ${reset}${orangebold}x${reset}
 ${orangebold}xxxxxxxxxxxx${reset}${orange}   ${black}Por${orange} ${black}lo${orange} ${black}tanto,${orange} ${black}se${orange} ${black}les${orange} ${black}requerirá${orange} ${black}que${orange} ${black}resuelvan${orange} ${black}correctamente${orange}          ${reset}${orangebold}xx${reset}
            ${orangebold}xx${reset}${orange}  ${black}12${orange} ${black}de${orange} ${black}los${orange} ${black}20${orange} ${black}problemas${orange} ${black}que${orange} ${black}el${orange} ${black}ordenador${orange} ${black}poseido${orange} ${black}les${orange} ${black}presentará.${orange}      ${reset}${orangebold}x${reset}
            ${orangebold}xx${reset}${orange}                                                                       ${reset}${orangebold}xx${reset}
            ${orangebold}xx${reset}${orange}  ${black}Esta${orange} ${black}tarea${orange} ${black}no${orange} ${black}será${orange} ${black}facil,${orange} ${black}pero${orange} ${black}confiamos${orange} ${black}en${orange} ${black}su${orange} ${black}habilidad${orange} ${black}y${orange} ${black}dedicación${orange} ${reset}${orangebold}xx${reset}
            ${orangebold}xx${reset}${orange}  ${black}para${orange} ${black}completarla.${orange} ${black}Les${orange} ${black}deseamos${orange} ${black}todo${orange} ${black}el${orange} ${black}éxito${orange} ${black}en${orange} ${black}su${orange} ${black}misión${orange} ${black}para${orange} ${black}libera${orange}  ${reset}${orangebold}x${reset}
            ${orangebold}xx${reset}${orange}  ${black}nuestro${orange} ${black}sistema${orange} ${black}de${orange} ${black}la${orange} ${black}IA${orange} ${black}malévola.${orange}                                     ${reset}${orangebold}xx${reset}
            ${orangebold}xx${reset}${orange}                                                                          ${reset}${orangebold}xx${reset}
            ${orangebold}xx${reset}${orange}  ${black}Atentamente,${orange}                                                             ${reset}${orangebold}xx${reset}
            ${orangebold}xx${reset}${orange}  ${black}El${orange} ${black}equipo${orange} ${black}de${orange} ${black}seguridad${orange} ${black}informática.${orange}                                       ${reset}${orangebold}x${reset}
            ${orangebold}xx${reset}${orange}  ${reset}${orangebold}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx${reset}
            ${orangebold}x${reset}${orange}  ${reset}${orangebold}xx${reset}${orange}   ${reset}${orangebold}xxx${reset}${orange}                                                                         ${reset}${orangebold}x${reset}
            ${orangebold}x${reset}${orange}  ${reset}${orangebold}x${reset}${orange}      ${reset}${orangebold}x${reset}${orange}                                                                         ${reset}${orangebold}xx${reset}
            ${orangebold}x${reset}${orange}  ${reset}${orangebold}xxxxx${reset}${orange}  ${reset}${orangebold}x${reset}${orange}                                                                          ${reset}${orangebold}xx${reset}
            ${orangebold}xx${reset}${orange}       ${reset}${orangebold}xx${reset}${orange}                                                                          ${reset}${orangebold}x${reset}
             ${orangebold}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx${reset}""")
        seconds = 0
        Thread.sleep(seconds * 1000L)
        println("Después de analizar la nota, los expertos en seguridad informática se dieron cuenta de que se trataba de una pista importante para la resolución")
        println("de un caso que habían estado investigando durante años: la presencia de una IA malévola que había infectado los sistemas de varias empresas de")
        println("la ciudad \n")
        seconds = 0
        Thread.sleep(seconds * 100L)
        i += 5
        problemas()
        println("El plan funcionó a la perfección. El virus logró neutralizar a la IA, y finalmente, el ordenador poseído quedó inactivo. El programador que había")
        println("luchado contra el ordenador durante años, finalmente había encontrado la paz. Se sentía aliviado y libre después de tanto tiempo de luchar ")
        println("contra la IA.\n")
        println("FIN")
    }
    /**
     * Esta función se imprime por pantalla si el usuario ha acertado mínimo 3 de los 5 problemas que te reta el programa.
     */
    fun virusSolucionado(){
        var seconds:Int
        seconds = 0
        Thread.sleep(seconds * 1000L)
        println("El ordenador se sacude, y la ventana emergente desaparece. Sonríes triunfantemente, creyendo que habías vencido al virus. Pero en este momento,\nsientes un escalofrío recorriendo por tu espalda. Y de repente…")
        seconds = 0
        Thread.sleep(seconds * 1000L)
        print("""
▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
▓                             ${green}╔╦╗╔═╗  ╔═╗╔═╗╦  ╦╔═╗╦╔╦╗╔═╗    ╦ ╦╔═╗╔═╗  ╔═╗╦ ╦╔═╗╔═╗╦═╗╔═╗╔╦╗╔═╗  ╔═╗╦    ╔╦╗╔═╗╔═╗╔═╗╔═╗╦╔═╗    ╔═╗╔═╗╦═╗╔═╗$reset ▓                                                                                                                                                                           
▓ ███████████████████████████  ${green}║ ║╣   ╠╣ ║╣ ║  ║║  ║ ║ ║ ║    ╠═╣╠═╣╚═╗  ╚═╗║ ║╠═╝║╣ ╠╦╝╠═╣ ║║║ ║  ║╣ ║     ║║║╣ ╚═╗╠═╣╠╣ ║║ ║    ╠═╝║╣ ╠╦╝║ ║$reset ▓                                                                                                         
▓ ███████▀▀▀░░░░░░░▀▀▀███████  ${green}╩ ╚═╝  ╚  ╚═╝╩═╝╩╚═╝╩ ╩ ╚═╝ ┘  ╩ ╩╩ ╩╚═╝  ╚═╝╚═╝╩  ╚═╝╩╚═╩ ╩═╩╝╚═╝  ╚═╝╩═╝  ═╩╝╚═╝╚═╝╩ ╩╚  ╩╚═╝ o  ╩  ╚═╝╩╚═╚═╝$reset ▓                                                                                                         
▓ ████▀░░░░░░░░░░░░░░░░░▀████       ${green}╔═╗╦ ╦╔═╗╦═╗╔═╗    ╔═╗╔═╗╦═╗╔═╗╔═╗  ╔╦╗╦  ╔═╗╔═╗╔═╗╦  ╔═╗╦  ╦╔═╗  ╔═╗╔═╗╦═╗  ╔═╗╦   ╦═╗╔═╗╔═╗╔╦╗╔═╗$reset        ▓
▓ ███│░░░░░░░░░░░░░░░░░░░│███       ${green}╠═╣╠═╣║ ║╠╦╝╠═╣    ╚═╗║╣ ╠╦╝╠═╣╚═╗  ║║║║  ║╣ ╚═╗║  ║  ╠═╣╚╗╔╝║ ║  ╠═╝║ ║╠╦╝  ║╣ ║   ╠╦╝║╣ ╚═╗ ║ ║ ║$reset        ▓
▓ ██▌│░░░░░░░░░░░░░░░░░░░│▐██       ${green}╩ ╩╩ ╩╚═╝╩╚═╩ ╩ ┘  ╚═╝╚═╝╩╚═╩ ╩╚═╝  ╩ ╩╩  ╚═╝╚═╝╚═╝╩═╝╩ ╩ ╚╝ ╚═╝  ╩  ╚═╝╩╚═  ╚═╝╩═╝ ╩╚═╚═╝╚═╝ ╩ ╚═╝$reset        ▓
▓ ██░└┐░░░░░░░░░░░░░░░░░┌┘░██   ${green}╔╦╗╔═╗  ╔╦╗╦ ╦  ╦  ╦╦╔╦╗╔═╗     ╦ ╦╔═╗  ╔╗╔╔═╗  ╔═╗╔═╗╦ ╦  ╦ ╦╔╗╔ ╦  ╦╦╦═╗╦ ╦╔═╗     ╔═╗╔═╗╦ ╦ ╦ ╦╔╗╔╔═╗$reset       ▓
▓ ██░░└┐░░░░░░░░░░░░░░░┌┘░░██   ${green} ║║║╣    ║ ║ ║  ╚╗╔╝║ ║║╠═╣     ╚╦╝║ ║  ║║║║ ║  ╚═╗║ ║╚╦╝  ║ ║║║║ ╚╗╔╝║╠╦╝║ ║╚═╗     ╚═╗║ ║╚╦╝ ║ ║║║║╠═╣$reset       ▓ 
▓ ██░░┌┘▄▄▄▄▄░░░░░▄▄▄▄▄└┐░░██   ${green}═╩╝╚═╝   ╩ ╚═╝   ╚╝ ╩═╩╝╩ ╩  o   ╩ ╚═╝  ╝╚╝╚═╝  ╚═╝╚═╝ ╩   ╚═╝╝╚╝  ╚╝ ╩╩╚═╚═╝╚═╝  ┘  ╚═╝╚═╝ ╩  ╚═╝╝╚╝╩ ╩$reset       ▓ 
▓ ██▌░│██████▌░░░▐██████│░▐██       ${green}╦╔╗╔╔╦╗╔═╗╦  ╦╔═╗╔═╗╔╗╔╔═╗╦╔═╗  ╔═╗╦═╗╔╦╗╦╔═╗╦╔═╗╦╔═╗╦    ╔═╗ ╦ ╦╔═╗ ╦ ╦╔═╗  ╔═╗╔═╗╔╦╗╔═╗╔╦╗╔═╗$reset            ▓
▓ ███░│▐███▀▀░░▄░░▀▀███▌│░███       ${green}║║║║ ║ ║╣ ║  ║║ ╦║╣ ║║║║  ║╠═╣  ╠═╣╠╦╝ ║ ║╠╣ ║║  ║╠═╣║    ║═╬╗║ ║║╣  ╠═╣╠═╣  ║╣ ╚═╗ ║ ╠═╣ ║║║ ║$reset            ▓
▓ ██▀─┘░░░░░░░▐█▌░░░░░░░└─▀██       ${green}╩╝╚╝ ╩ ╚═╝╩═╝╩╚═╝╚═╝╝╚╝╚═╝╩╩ ╩  ╩ ╩╩╚═ ╩ ╩╚  ╩╚═╝╩╩ ╩╩═╝  ╚═╝╚╚═╝╚═╝ ╩ ╩╩ ╩  ╚═╝╚═╝ ╩ ╩ ╩═╩╝╚═╝$reset            ▓
▓ ██▀─┘░░░░░░░▐█▌░░░░░░░└─▀██   ${green}╔═╗╔═╗╔═╗╔═╗╦═╗╔═╗╔╗╔╔╦╗╔═╗  ╔═╗  ╔═╗╦  ╔═╗╦ ╦╦╔═╗╔╗╔  ╦  ╔═╗  ╔═╗╦ ╦╔═╗╦╔═╗╦╔═╗╔╗╔╔╦╗╔═╗╔╦╗╔═╗╔╗╔╔╦╗╔═╗$reset       ▓
▓ ██▀─┘░░░░░░░▐█▌░░░░░░░└─▀██   ${green}║╣ ╚═╗╠═╝║╣ ╠╦╝╠═╣║║║ ║║║ ║  ╠═╣  ╠═╣║  ║ ╦║ ║║║╣ ║║║  ║  ║ ║  ╚═╗║ ║╠╣ ║║  ║║╣ ║║║ ║ ║╣ ║║║║╣ ║║║ ║ ║╣$reset        ▓ 
▓ █████░░▐█─┬┬┬┬┬┬┬─█▌░░█████   ${green}╚═╝╚═╝╩  ╚═╝╩╚═╩ ╩╝╚╝═╩╝╚═╝  ╩ ╩  ╩ ╩╩═╝╚═╝╚═╝╩╚═╝╝╚╝  ╩═╝╚═╝  ╚═╝╚═╝╚  ╩╚═╝╩╚═╝╝╚╝ ╩ ╚═╝╩ ╩╚═╝╝╚╝ ╩ ╚═╝$reset       ▓
▓ █████░░▐█─┬┬┬┬┬┬┬─█▌░░█████ ${green}╔╗ ╦═╗╦╦  ╦  ╔═╗╔╗╔╔╦╗╔═╗  ╔═╗╔═╗╔╦╗╔═╗  ╔╦╗╦ ╦  ╔═╗╔═╗╦═╗╔═╗  ╦  ╦╔╗ ╔═╗╦═╗╔═╗╦═╗╔╦╗╔═╗      ╦╦ ╦╔╗╔╔╦╗╔═╗╔═╗$reset   ▓
▓ █████▄░░░└┴┴┴┴┴┴┴┘░░░▄█████ ${green}╠╩╗╠╦╝║║  ║  ╠═╣║║║ ║ ║╣   ║  ║ ║║║║║ ║   ║ ║ ║  ╠═╝╠═╣╠╦╝╠═╣  ║  ║╠╩╗║╣ ╠╦╝╠═╣╠╦╝║║║║╣       ║║ ║║║║ ║ ║ ║╚═╗$reset   ▓
▓ ███████▄░░░░░░░░░░░▄███████ ${green}╚═╝╩╚═╩╩═╝╩═╝╩ ╩╝╚╝ ╩ ╚═╝  ╚═╝╚═╝╩ ╩╚═╝   ╩ ╚═╝  ╩  ╩ ╩╩╚═╩ ╩  ╩═╝╩╚═╝╚═╝╩╚═╩ ╩╩╚═╩ ╩╚═╝  o  ╚╝╚═╝╝╚╝ ╩ ╚═╝╚═╝  ┘$reset▓
▓ ██████████▄▄▄▄▄▄▄██████████    ${green}╔╦╗╔═╗╔╦╗╦╔╗╔╔═╗╦═╗╔═╗╔╦╗╔═╗╔═╗  ╔═╗╦    ╔╦╗╦ ╦╔╗╔╔╦╗╔═╗  ╔╦╗╔═╗  ╦  ╔═╗  ╔╦╗╔═╗╔═╗╔╗╔╔═╗╦  ╔═╗╔═╗╦╔═╗$reset        ▓
▓ ███████████████████████████     ${green}║║║ ║║║║║║║║╠═╣╠╦╝║╣ ║║║║ ║╚═╗  ║╣ ║    ║║║║ ║║║║ ║║║ ║   ║║║╣   ║  ╠═╣   ║ ║╣ ║  ║║║║ ║║  ║ ║║ ╦║╠═╣$reset        ▓
▓                                ${green}═╩╝╚═╝╩ ╩╩╝╚╝╩ ╩╩╚═╚═╝╩ ╩╚═╝╚═╝  ╚═╝╩═╝  ╩ ╩╚═╝╝╚╝═╩╝╚═╝  ═╩╝╚═╝  ╩═╝╩ ╩   ╩ ╚═╝╚═╝╝╚╝╚═╝╩═╝╚═╝╚═╝╩╩ ╩  o$reset     ▓
▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
""")
        seconds = 0
        Thread.sleep(seconds * 1000L)
        println("Te das cuenta de que habías subestimado al ordenador poseído. Habías caído en una trampa aún peor que la que los trabajadores te habían advertido.\nAhora, estás atrapado en una lucha constante contra el ordenador, intentando mantenerlo bajo control y proteger al mundo de su poder malévolo.\nDesde entonces, tendrás que esperar a que el ordenador poseído, te proponga más ejercicios para poder luchar por el control absoluto de la \ntecnología.")
    }
    fun mostrarproblemas(tema:String){
        val problemas = problemasAlumno.ordenarPor(tema)
        var i = 1
        for (problema in problemas) {
            problema.imprimirProblema(i)
            i++
        }
        do {
            println("$bold${white}¿Quieres resolver algún problema?".uppercase())
            println("En caso afirmativo, escríbenos su número, en cualquier otro caso escribe 'no': $reset".uppercase())
            var respuestaUsuario = scanner.next().uppercase()
            if(respuestaUsuario == "NO") menu()
            else if(respuestaUsuario.toInt() in 1 ..   problemas.size){
                problemas[respuestaUsuario.toInt()-1].resolverProblema()
            }else respuestaUsuario = "error"
        }while (respuestaUsuario == "error")
    }
    fun guardarHistorial(){
        val problemas = problemasAlumno.resueltos(false)
        val gson = Gson()

        // Crear el archivo de historial con el nombre del correo del alumno
        val archivo = File("src/main/kotlin/historial/historial_${correoAlumno}.json")

        for (problema in problemas) {
            problema.imprimirEstat()

            // Convertir el objeto Prblema a formato JSON y escribirlo al final del archivo
            val historialJson = gson.toJson(problema)
            archivo.appendText("${historialJson}\n")

            println("${green}Historial de la partida guardada satisfactoriamente$reset")
        }
    }
    fun problemas(){
        val problemas = this.problemasAlumno.resueltos(true)
        do{
            problemas[i].imprimirProblema(i+1)
            println("${white}Quieres resolver-lo?$reset".uppercase())
            println("""$bold$box$green SI $reset  $bold$box$redBold NO $reset""")
            do{
                print("${white}Elige una opción válida:   $reset".uppercase())
                val respuestaUsuario = scanner.next().uppercase()
                if(respuestaUsuario == "SI" || respuestaUsuario == "SÍ"){
                    println()
                    problemas[i].resolverProblema()
                    problemasResueltos++
                    contadorproblemas++
                    i++
                }else if(respuestaUsuario == "NO") {
                    println()
                    contadorproblemas++
                    i++
                }
                else respuestaUsuario == "Error"
            }while (respuestaUsuario=="Error")
        }while (i in problemas.indices)
    }
}


