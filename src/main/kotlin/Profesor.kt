import com.google.gson.Gson
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import kotlin.system.exitProcess


var usuarioAlumno = ""
class Profesor(val nombre: String, var correo: String, var contraseña: String) {
    fun crearcuenta() {
        // 1. Leer el contenido del archivo JSON en un objeto de clase Kotlin
        val gson = Gson()
        val archivo = File("./src/main/kotlin/Profesor.json")
        val contenido = archivo.readText()
        val listaProfesor = try {
            gson.fromJson(contenido, Array<Profesor>::class.java).toMutableList()
        } catch (ex: Exception) {
            mutableListOf<Profesor>()
        }

        // 2. Crear un nuevo objeto Alumno con un correo electrónico específico
        val nuevoProfesor = Profesor("$nombre", "$correo", "$contraseña")

        // 3. Verificar si el correo electrónico ya existe en la lista de alumnos
        if (listaProfesor.any { it.correo == nuevoProfesor.correo }) {
            println("${redBold}Este correo ya pertenece a un usuario, por favor inicia sesión.$reset")
            println("${white}Dirigido a la pantalla de iniciar sesion$reset")
            iniciarsesion()
            return
        }

        // 4. Agregar el nuevo objeto Alumno a la lista de alumnos
        listaProfesor.add(nuevoProfesor)

        // 5. Escribir el contenido actualizado de la lista de alumnos de vuelta al archivo JSON
        val contenidoActualizado = gson.toJson(listaProfesor)
        archivo.writeText(contenidoActualizado)

        // 6. Imprimir el correo electrónico del nuevo alumno agregado
        println("${green}El nuevo profesor agregado, llamado ${nuevoProfesor.nombre} con el correo electrónico: ${nuevoProfesor.correo}$reset")
    }

    fun iniciarsesion() {
        // 1. Leer el contenido del archivo JSON en un objeto de lista de objetos Alumno
        val gson = Gson()
        val archivo = File("./src/main/kotlin/Profesor.json")
        val contenido = archivo.readText()
        val listaprofesor = gson.fromJson(contenido, Array<Profesor>::class.java).toList()

        // 2. Pedir al usuario que introduzca su correo electrónico y contraseña
        print("${white}Introduce tu correo electrónico: ")
        val correoBusqueda = readLine()?.trim() ?: ""

        print("Introduce tu contraseña: $reset")
        val contrasenaBusqueda = readLine()?.trim() ?: ""

        // 3. Buscar en la lista de objetos Alumno si existe algún objeto que contenga el correo electrónico introducido por el usuario
        val profesorCoincidente =
            listaprofesor.find { profesor -> profesor.correo == correoBusqueda && profesor.contraseña == contrasenaBusqueda }

        // 4. Si existe un objeto Alumno con el correo electrónico introducido por el usuario, saludar al usuario
        if (profesorCoincidente != null) {
            println("Hola ${profesorCoincidente.nombre.uppercase()}!")
            menu()
        } else {
            println("${redBold}No se encontró ningún profesor con el correo electrónico introducido.$reset")
            iniciarsesion()
        }
    }

    fun menu() {
        println("$bold${white}")
        println("MENU:".uppercase())
        println("     1- Añadir nuevos problemes ".uppercase())
        println("     2- Obtener un informe del trabajo del alumno ".uppercase())
        println("     3- Cambiar de cuenta ".uppercase())
        println("     4- Salir ".uppercase())
        println(reset)
        do {
            print("${mgreen}Elige una opción válida: $reset".uppercase())
            var opcioUsuari = scanner.next()
            when (opcioUsuari) {
                "1" -> {
                    println()
                    afegirProblema()
                    println()
                    menu()
                }

                "2" -> {
                    println()
                    println("${white}Introduce el mail del alumno que quieres reportar:$reset")
                    var correoAlumno: String = scanner.next()
                    usuarioAlumno += correoAlumno
                    reportAlumne()
                    println()
                    menu()
                }
                "3"->{
                    println()
                    main()
                }
                "4" -> {
                    println()
                    println("${white}Hasta luego!$reset")
                    exitProcess(0)
                }

                else -> opcioUsuari = "error"
            }
        } while (opcioUsuari == "error")
    }

    fun afegirProblema() {
        val atributsProblemaNom =
            listOf("tema", "titulo", "enunciado", "input publico", "output publico", "input privado", "output privado")
        val atributsProblemaValor = mutableListOf<String>()
        for (atribut in atributsProblemaNom) {
            print("${white}Introduce $atribut: $reset".uppercase())
            val inputUsuari = scanner.next() + scanner.nextLine()
            atributsProblemaValor.add(inputUsuari)
        }
        val problema = Problema(
            atributsProblemaValor[0],
            atributsProblemaValor[1],
            atributsProblemaValor[2],
            atributsProblemaValor[3],
            atributsProblemaValor[4],
            atributsProblemaValor[5],
            atributsProblemaValor[6]
        )
        val problemaSerialitzat = Json.encodeToString(problema)
        File("./src/main/kotlin/problemas.json").appendText("$problemaSerialitzat\n")
    }

    fun reportAlumne() {
        val archivo = File("src/main/kotlin/historial/historial_${usuarioAlumno}.json")
        val contenido = archivo.readLines()
        println("${white}Datos a reportar:".uppercase())
        println("     1- Sacar una puntuación en función de los problemas resueltos".uppercase())
        println("     2- Descontar por intentos".uppercase())
        println("     3- Mostrarlo de manera gràfica".uppercase())
        println("     4- Mostral el historial del alumno".uppercase())
        println("     5- Volver al menu$reset".uppercase())
        do {
            print("${mgreen}Elige una opción válida: $reset".uppercase())
            var opcioUsuari = scanner.next()
            when (opcioUsuari) {
                "1" -> {
                    println()
                    var contadorProblemas = 0
                    for (line in contenido) {
                        contadorProblemas++
                    }
                    println("${green}Puntuación total = ${(contadorProblemas * 10) / 20}/10$reset".uppercase())
                    println()
                    reportAlumne()
                }

                "2" -> {
                    println()
                    for (line in contenido) {
                        val problema = Json.decodeFromString<Problema>(line)
                        print("${white}Título: ${problema.titulo}  ||".uppercase())
                        if (problema.intentos == 1) {
                            println("\t${green}Puntuación = ${10}$reset".uppercase())
                        } else {
                            println("\t${green}Puntuación = ${10 - problema.intentos}$reset".uppercase())
                        }
                    }
                    println()
                    reportAlumne()
                }

                "3" -> {
                    println()
                    for (line in contenido) {
                        val problema = Json.decodeFromString<Problema>(line)
                        print("${white}Título: ${problema.titulo}  ||  $reset".uppercase())
                        if (problema.intentos == 1) {
                            repeat(10) {
                                print("${green}✸$reset")
                            }
                        } else {
                            repeat(10 - problema.intentos) {
                                print("${green}✸$reset")
                            }
                        }
                        println()
                    }
                    println()
                    reportAlumne()
                }

                "4" -> {
                    println()
                    for (line in contenido) {
                        val problema = Json.decodeFromString<Problema>(line)
                        println("${box} Título: ${problema.titulo} ")
                        println(" NºIntentos: ${problema.intentos} ")
                        println(" Intentos: ${problema.intentos_usuario} ")
                        println(" Resuelto: ${problema.resuelto}\n $reset")
                    }
                    println()
                    menu()
                }

                "5" -> {
                    println()
                    menu()
                }

                else -> opcioUsuari = "error"
            }
        } while (opcioUsuari == "error")
    }
}